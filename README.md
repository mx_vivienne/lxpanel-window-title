# lxpanel-window-title
an lxpanel plugin to display the title of the currently active window

# Usage:
+ get the ev.h header from https://github.com/lxde/lxpanel/tree/master/src and paste it here
+ compile with `make`
+ copy `wtitle.so` file to `/usr/lib/x86_64-linux-gnu/lxpanel/plugins/`
+ restart lxpanel with `lxpanelctl restart`
+ right-click your panel and add the plugin
