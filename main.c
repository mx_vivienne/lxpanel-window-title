#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <glib/gi18n.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>

#include <lxpanel/plugin.h>
#include <lxpanel/misc.h>
#include "ev.h"

/* Private context for window title plugin. */
typedef struct {
    LXPanel * panel;			/* Back pointer to Panel */
    config_setting_t * settings;
    unsigned int timer;
    GtkWidget * label;			/* The label */
    gboolean bold;			/* User preference: True if bold font */
} WindowTitle;

static void window_title_destructor(gpointer user_data);

/* Handler for current_desktop event from window manager. */
static gboolean window_title_redraw(WindowTitle * wt)
{
    /* Get ID of currently active window, None if no focused window */
    Window * active = get_xaproperty(GDK_ROOT_WINDOW(), a_NET_ACTIVE_WINDOW, XA_WINDOW, 0);

    /* Get Title */
    char * title = "";
    if (None != active) 
    {
	title = get_textproperty(*active, a_NET_WM_VISIBLE_NAME);
    }

    /* redraw */
    lxpanel_draw_label_text(wt->panel, wt->label, title, wt->bold, 1, TRUE);

    return TRUE;
}

/* Plugin constructor. */
static GtkWidget *window_title_constructor(LXPanel *panel, config_setting_t *settings)
{
    /* Allocate plugin context and set into Plugin private data pointer. */
    WindowTitle * wt = g_new0(WindowTitle, 1);
    GtkWidget *p;
    int tmp_int;

    g_return_val_if_fail(wt != NULL, 0);
    wt->panel = panel;
    wt->settings = settings;

    /* Default parameters. */
    wt->bold = FALSE;

    /* Load parameters from the configuration file. */
    if (config_setting_lookup_int(settings, "BoldFont", &tmp_int))
        wt->bold = tmp_int != 0;

    /* Allocate top level widget and set into Plugin widget pointer. */
    p = gtk_event_box_new();
    lxpanel_plugin_set_data(p, wt, window_title_destructor);

    /* Allocate label widget and add to top level. */
    wt->label = gtk_label_new(NULL);
    gtk_container_add(GTK_CONTAINER(p), wt->label);

    wt->timer = g_timeout_add(100, (GSourceFunc) window_title_redraw, (gpointer) wt);

    /* Initialize value and show the widget. */
    window_title_redraw(wt);
    gtk_widget_show_all(p);
    return p;
}

/* Plugin destructor. */
static void window_title_destructor(gpointer user_data)
{
    WindowTitle * wt = (WindowTitle *) user_data;
    g_source_remove(wt->timer);
}

/* Callback when the configuration dialog has recorded a configuration change. */
static gboolean window_title_apply_configuration(gpointer user_data)
{
    WindowTitle * wt = lxpanel_plugin_get_data(user_data);
    window_title_redraw(wt);
    config_group_set_int(wt->settings, "BoldFont", wt->bold);
    return FALSE;
}

/* Callback when the configuration dialog is to be shown. */
static GtkWidget *window_title_configure(LXPanel *panel, GtkWidget *p)
{
    WindowTitle * wt = lxpanel_plugin_get_data(p);
    GtkWidget * dlg = lxpanel_generic_config_dlg(_("Plugin Settings"),
        panel, window_title_apply_configuration, p,
        _("Bold font"), &wt->bold, CONF_TYPE_BOOL,
        NULL);
    gtk_widget_set_size_request(GTK_WIDGET(dlg), 400, -1);	/* Improve geometry */
    return dlg;
}

/* Callback when panel configuration changes. */
static void window_title_panel_configuration_changed(LXPanel *panel, GtkWidget *p)
{
    WindowTitle * wt = lxpanel_plugin_get_data(p);
    window_title_redraw(wt);
}

FM_DEFINE_MODULE(lxpanel_gtk, window_title)

/* Plugin descriptor. */
LXPanelPluginInit fm_module_init_lxpanel_gtk = {
    .name = N_("Active Window Title (test)"),
    .description = N_("Displays title of currently active window"),

    .new_instance = window_title_constructor,
    .config = window_title_configure,
    .reconfigure = window_title_panel_configuration_changed,
};
